# sBotto

The code for the anarchist, noise-making cousin of [Botto](https://www.opendotlab.it/portfolio-item/botto/), existing only for a single, chaotic performance, during the _Gara delle Batterie Elettroniche_ @ **Cascina Torchiera** on May 22nd 2022.

## Working

- Push the button to emit a sound chosen randomly among a set of frequencies (with an added random variation on the pitch just for the sake of chaos)
- Turn the encoder to increase or decrease the tremolo effect.
- That's it.

___
Needless to say that this thing goes off with a [CC0 licence](https://creativecommons.org/publicdomain/zero/1.0/)

![CC0 licence](https://i.creativecommons.org/p/zero/1.0/88x31.png)