#include <Tone32.h>
#include <stdlib.h>

// Custom libraries
#include "src/RGBEntity/RGBEntity.h"
#include "src/Button/Button.h"
#include "src/Encoder/Encoder.h"
#include "src/Display/Display.h"

// How is it that you have object- and logic- oriented classes for all the devices but the speaker!??!
// Com'on!

// Hardware
#define BUTTON_PIN 13
#define SPEAKER_PIN 17
#define SPEAKER_CHANNEL 0
#define RESOLUTION 8
#define VOLUME 70

RGBEntity led(14, 16, 15, 1, 2, 3, 2000, 8, true);
RGBEntity lcd(6, 8, 7, 4, 5, 6, 2000, 8);

Button button(BUTTON_PIN);

Encoder encoder(41, 42);
void IRAM_ATTR isr() { encoder.encoderStep(); }; // trick for associating a class function as an interrupt function

Display display(0, 1, 2, 3, 4, 5);

String displayBuffer[4];

enum Colors { BLACK, WHITE, RED, GREEN, BLUE, LIGHT_WHITE,  EXTRA_1,  EXTRA_2,  EXTRA_3,  EXTRA_4,  EXTRA_5,  EXTRA_6,  EXTRA_7,  EXTRA_8,  EXTRA_9,  EXTRA_10,  EXTRA_11,  EXTRA_12,  EXTRA_13,  EXTRA_14,  EXTRA_15, SIZE_OF_COLORS };
short colors[SIZE_OF_COLORS][3] = {
  {0, 0, 0},            //BLACK
  {205, 205, 205},      //WHITE
  {255, 30, 30},        //RED
  {35, 255, 35},        //GREEN
  {35, 35, 255},        //BLUE
  {10, 10, 10},          //LIGHT_WHITE
  {255, 0, 0},          // EXTRA_1
  {0, 255, 0},          // EXTRA_2
  {255, 255, 0},        // EXTRA_3
  {0, 0, 255},          // EXTRA_4
  {255, 0, 255},        // EXTRA_5
  {0, 255, 255},        // EXTRA_6
  {255, 128, 128},      // EXTRA_7
  {128, 255, 128},      // EXTRA_8
  {255, 255, 128},      // EXTRA_9
  {128, 128, 255},      // EXTRA _10
  {255, 128, 255},      // EXTRA _11
  {128, 255, 255},      // EXTRA _12
  {128, 128, 128},      // EXTRA _13
  {0, 128, 128},        // EXTRA _14
  {128, 128, 0}         // EXTRA _15
};

Colors ledColor;
Colors lcdColor;

// This is overtly complicated and unnecessary
struct Note {
  float start;
  unsigned int pitch;  
};

#define VARIANCE 2
#define NUM_NOTES 13
unsigned int notes[NUM_NOTES] = {
  100, 
  133, 
  149, 
  200, 
  266, 
  298, 
  400, 
  533, 
  599, 
  800, 
  1064, 
  1198, 
  1600
};

Note playingNote;
bool playingFlag;

#define TREMOLO_MAX 750
#define TREMOLO_STEP_1 2
#define TREMOLO_STEP_2 10
#define TREMOLO_STEP_3 50
#define TREMOLO_THRESHOLD_1 50
#define TREMOLO_THRESHOLD_2 300
unsigned int tremoloTempo;
unsigned long tremoloStart;
bool tremoloFlag;

void setup() {
  Serial.begin(115200);

  ledcSetup(SPEAKER_CHANNEL, 600, RESOLUTION);
  ledcAttachPin(SPEAKER_PIN, SPEAKER_CHANNEL);

  attachInterrupt(digitalPinToInterrupt(encoder.interruptPin()), isr, CHANGE);

  display.begin(20, 4);

  // Start state
  playingFlag = false;
  tremoloTempo = 0;
  tremoloFlag = false;

  Serial.println("Evvai di sBotto");
}

void loop() {
  readInputs();
  updateSystem();
  renderSbotto();
}

void readInputs() {
  // Button behavior
  if (button.value()) {
    if (button.isState(buttonState::IDLE)) {
      // Just pressed
      button.setState(buttonState::JUST_PRESSED);
      unfinishedNoteFlag = true;
    } else {
      // Pressed
      button.setState(buttonState::PRESSED);
    }
  } else if (button.isState(buttonState::PRESSED)) {
    // Released
    button.setState(buttonState::RELEASED);
  } else if (button.isState(buttonState::RELEASED)) {
    // Idle
    button.setState(buttonState::IDLE);
  }

  // if encoder is operated, change the tremolo
  if (encoder.operated()) {
    // Non-linear behavior of tremolo operand
    int tmp = 0;
    if (tremoloTempo >= TREMOLO_THRESHOLD_1 && tremoloTempo <= TREMOLO_THRESHOLD_2) {
      tmp = TREMOLO_STEP_2;
    } else if (tremoloTempo >= TREMOLO_THRESHOLD_2) {
      tmp = TREMOLO_STEP_3;
    } else {
      tmp = TREMOLO_STEP_1;
    }
    if (encoder.direction()) {      
      if (tremoloTempo <= TREMOLO_MAX - TREMOLO_STEP_1) {     
        tremoloTempo += tmp;
      }
    } else {
      if (tremoloTempo >= TREMOLO_STEP_1) {
        tremoloTempo -= tmp;        
      }
    }
  }
}

void updateSystem() {
  // Update audio
  if (button.isState(buttonState::JUST_PRESSED)) {
    Note tmp;    
    tmp.start = millis();
    tmp.pitch = notes[rand() % NUM_NOTES] + (VARIANCE - (rand() % (VARIANCE * 2)));
    playingNote = tmp;
    playingFlag = true;    
  } else if (button.isState(buttonState::RELEASED)) {
    playingFlag = false;
  }

  // Update display
  String tmp;
  tmp = button.isState(buttonState::PRESSED) ? "      Pressed!" : "";
  displayBuffer[0] = tmp;
  tmp = "Frequency: ";
  tmp.concat(button.isState(buttonState::PRESSED) ? playingNote.pitch : 0 );
  tmp.concat(" Hz");
  displayBuffer[1] = tmp;
  tmp = "Tremolo: ";
  tmp.concat(tremoloTempo);
  displayBuffer[2] = tmp;
  displayBuffer[3] = "";

  // Update led
  if (button.isState(buttonState::JUST_PRESSED)) {
    ledColor = static_cast<Colors>(rand() % Colors::SIZE_OF_COLORS);
  } else if (button.isState(buttonState::RELEASED)) {    
    ledColor = Colors::BLACK;
    led.turnOn();
  }
}

void renderSbotto() {
  // Render audio
  if (playingFlag) {
    if (button.isState(buttonState::JUST_PRESSED)) {
      playNote(SPEAKER_CHANNEL, playingNote.pitch);
      tremoloStart = millis();
    } else {
      // tremolo effect
      if (tremoloTempo) {
        if (millis() - tremoloStart >= tremoloTempo) {
          if (tremoloFlag) {
            mute(SPEAKER_CHANNEL);
          } else {
            setVolume(SPEAKER_CHANNEL, VOLUME);
          }
          tremoloStart = millis();
          tremoloFlag = !tremoloFlag;
        }
      }
    }
  } else {
    mute(SPEAKER_CHANNEL);
  }

  // Render display
  display.clear();
  for (short i = 0; i < 4; i++) {
    display.setCursor(0, i);
    display.print(displayBuffer[i]);
  }
 
  // Render led
  led.setColor(colors[ledColor]);
  if (tremoloFlag) {
    led.turnOff();
  } else {
    led.turnOn();
  }
}

void playNote(int channel, int pitch) {
  ledcSetup(channel, pitch, RESOLUTION);
  ledcWrite(channel, VOLUME);
}

void mute(int channel) {
  ledcWrite(channel, 0);
}

void setVolume(int channel, int vol) {
  ledcWrite(channel, vol);
}