/*
// Class to handle negative and positive logic momentary switch
*/

#ifndef BUTTON
#define BUTTON

#include "Arduino.h"

enum buttonState { IDLE, JUST_PRESSED, PRESSED, RELEASED };

class Button {
  public:
    Button(short pin);
    Button(short pin, bool logic); // if logic not provided, negative logic assumed

    bool value(); // true if active
    buttonState getState();
    bool isState(buttonState state);
    void setState(buttonState state);

  private:
    void _setup();
    
    short _pin;
    bool _logic; // false for pull-up; true for pull-down
    buttonState _state;
};

#endif