#include "Button.h"

Button::Button(short pin)
: _pin(pin), _logic(false), _state(buttonState::IDLE) {  
  _setup();
}

Button::Button(short pin, bool logic)
: _pin(pin), _logic(logic), _state(buttonState::IDLE) {
  _setup();
}

void Button::_setup() {
  if (_logic) {
    pinMode(_pin, INPUT);
  } else {
    pinMode(_pin, INPUT_PULLUP);
  }
}

bool Button::value() {
  return _logic ? digitalRead(_pin) : !digitalRead(_pin);
}

buttonState Button::getState() {
  return _state;
}

void Button::setState(buttonState state) {
  _state = state;
}

bool Button::isState(buttonState state) {
  return _state == state;
}